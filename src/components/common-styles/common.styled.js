import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const NavLink = styled(Link)`
display: inline-block;
text-decoration: none;
margin: 0;
border-radius: 3px;
background-color: #FFF44F;
box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
color: #000;
padding: 10px 20px;
cursor: pointer;
font-size: 25px;
:hover {
    color: #fff;
    transition: color 1s .2s linear;
}
`;

export const Main = styled.main`
width: 100%;
height: 100vh;
background-image: linear-gradient(
    to right,
    rgba(0, 0, 0, 0.8),
    rgba(0, 0, 0, 0.8)
  ), url(${props => props.homepage || props.user});
background-size: cover;
background-repeat: no-repeat;
font-family: 'Lato', sans-serif;
`;

export const Container = styled.div`
padding: 30px 100px;
`;