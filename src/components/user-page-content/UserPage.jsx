import {Container, UserPageTitle, SearchForm, SearchInput, SearchButton} from './userPage.styled';
import searchBtn from '../../assets/search-icon.svg';
import { useState } from 'react';
import fetchShows from '../../services/fetchShows';
import ShowsGallery from '../resultsList/ResultList';
import LoaderMark from '../loader/Loader';



export const UserPageContent = () => {
    const [searchQuery, setSearchQuery] = useState("");
    const [shows, setShows] = useState([]);
    const [result, setResult] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const hanldeChange = (e) => {
        const { value } = e.target;
        setSearchQuery(value);
    }

    const scrollToResults = () => {
        window.scrollTo({
            top: 1000,
          behavior: "smooth"
        });
      };

const loadShows = (e) => {
    e.preventDefault();
    setIsLoading(true)
    fetchShows(searchQuery)
    .then(data => {
        let res = [];
        data.map(el => {
            res.push(el.show);
           return res;
        })
        setShows(res);
        setResult(true);
        scrollToResults();
    })
    .catch(err => console.log(err))
    .finally(setSearchQuery(''), 
    setShows([]))
}
    return(
        <>
        <Container>
        <UserPageTitle>Find your favourite TV-show!</UserPageTitle>
        <SearchForm onSubmit={loadShows}>
            <SearchInput type="text" onChange={hanldeChange} value={searchQuery}></SearchInput>
            <SearchButton>
                <img src={searchBtn} alt="search" />
            </SearchButton>
        </SearchForm>
        {isLoading && <LoaderMark></LoaderMark>}
        </Container>
            {result && 
            <ShowsGallery arr={shows}>
            </ShowsGallery>}
        </>
    )
}