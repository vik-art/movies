import styled from 'styled-components';

const Container = styled.div`
width: 100%;
height: 100%;
display: flex;
align-items: center;
flex-direction: column;
background-color: #000;
justify-content: center;
`;

const UserPageTitle = styled.h1`
margin: 0;
color: #FFF;
font-size: 72px;
`
const SearchForm = styled.form`
width: 100%;
text-align: center;
`


const SearchInput = styled.input`
width: 50%;
height: 40px;
background-color: transparent;
border: none;
border-bottom: 1px solid #fff;
outline: none;
text-align: center;
color: #fff;
font-size: 35px;
letter-spacing: 3px;
`;

const SearchButton = styled.button`
background-color: transparent;
border: none;
margin-left: -60px;
cursor: pointer;
`

export {Container, UserPageTitle, SearchForm, SearchInput, SearchButton}