import { createPortal } from 'react-dom';

import {Overlay, ModalWindow, CloseButton, ModalTitle, ModalText} from './modal.styled';
import onCloseIcon from '../../assets/close-icon.svg';
import {NavLink} from '../../components';

const modalRoot = document.querySelector("#modal-root")

export const Modal = (props) => {    
      const handleBackdrop = (e) => {
        if (e.currentTarget === e.target) {
          props.onClose();
        }
      };
        return createPortal (
        <Overlay onClick={handleBackdrop}>
        <ModalWindow>
            <CloseButton onClick={props.onClose}>
                <img src={onCloseIcon} alt='close' width='30' height='30'/>
            </CloseButton>
            <ModalTitle>Warning!</ModalTitle>
            <ModalText>{props.text}</ModalText>
            <NavLink to={props.link}>{props.modalText}</NavLink>
        </ModalWindow>
    </Overlay>,
    modalRoot)
    }