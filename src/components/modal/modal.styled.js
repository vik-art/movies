import styled from 'styled-components';

const Overlay = styled.div`
    width: 100%;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.7);
`

const ModalWindow = styled.div`
    width: 400px;
    height: 300px;
    padding: 20px;
    background-color: #000;
    position: absolute;
    border-radius: 3px;
    top: 10%;
    right: 10%;
    border: 1px solid #FFF44F;
    font-family: 'Lato', sans-serif;
`

const CloseButton = styled.button`
display: block;
margin-left: 370px;
margin-top: -5px;
background-color: transparent;
border: none;
cursor: pointer;
&img {
    display: block;
    margin: 0;
}
`

const ModalTitle = styled.h2`
color: #FFF44F;
text-align: center;
margin: 0;
font-size: 30px;
`

const ModalText = styled.p`
color: #fff;
font-size: 20px;
line-height: 1.5;
`

export {Overlay, ModalWindow, CloseButton, ModalTitle, ModalText}