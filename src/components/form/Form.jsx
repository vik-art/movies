import { Button } from "../../components";
import {FormWindow, FormTitle, FormField, FieldContainer} from './form.styled';


export const Form = (props) => {
    const handleChange = e => {
        const { name, value } = e.target;
        switch(name) {
            case "email":
                props.emailFunc(value); 
                break; 
                case "password":
                    props.passwordFunc(value);
                    break;
                    default:
                    console.log("No such field type!")
        }
    }  

    return (
        <FormWindow>
        <FormTitle>{props.text}</FormTitle>
        <form onSubmit={props.func}>
            <FieldContainer>
            <FormField 
            type="email"
             name="email"
             value={props.email}
             onChange={handleChange}
             placeholder="Enter your email adress"
            
             />
            </FieldContainer>
            <FieldContainer>
            <FormField type="password"
             name="password"
             pattern="(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*"
             value={props.password}
             onChange={handleChange}
             placeholder="Enter your password (at least 6 characters)"
              />
            </FieldContainer>
                  <Button text="Submit">Submit</Button>
                </form>
        </FormWindow>
    )
}