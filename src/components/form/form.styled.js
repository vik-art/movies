import styled from "styled-components";

const FormWindow = styled.div`
width: 40%;
background-color: #000;
color: #fff;
height: 400px;
border-radius: 3px;
position: absolute;
top: 50%;
left: 50%;
text-align: center;
transform: translate(-50%, -50%);
box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
`;

const FormTitle = styled.h4`
font-size: 35px;
`;

const FormField = styled.input`
width: 70%;
height: 40px;
padding: 10px;
font-style: italic;
`;

const FieldContainer = styled.fieldset`
border: none;
`;

export {FormWindow, FormTitle, FormField, FieldContainer};