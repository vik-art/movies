import { signOut  } from "firebase/auth";
import { useContext } from "react";

import authContext from '../../context/auth/context';
import { auth } from "../../libs/firebase";

import { Logo, Button, NavLink } from '../../components';

import {HeaderContainer, Navigation, ButtonContainer, UserMenuItem, UserMenu} from './header.styled';

export const Header = () => {
    const { isLoggedIn, onLogOut } = useContext(authContext);


    const logOutFunction = e => {
        e.preventDefault();
       return signOut(auth).then(() => {
        onLogOut();
      })
    }

    return(
    <HeaderContainer id="header-js">
            <Navigation>
                <Logo link="/"></Logo>
                {isLoggedIn ? <UserMenu>
                    <UserMenuItem to="./favourite" >Favourite</UserMenuItem>
                    <UserMenuItem to="./" >Selected</UserMenuItem>
                    <UserMenuItem to="./" >Friends</UserMenuItem>
                    <Button func = {logOutFunction} text="Log Out"></Button>
            </UserMenu> :                
            <ButtonContainer>
                <NavLink to="/registration">Sing Up</NavLink>
                <NavLink to="/login">Log in</NavLink>
                </ButtonContainer>}
            </Navigation>
    </HeaderContainer>)
}