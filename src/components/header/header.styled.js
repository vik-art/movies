import styled from 'styled-components';
import { NavLink } from 'react-router-dom'

const HeaderContainer = styled.header`
width: 100%;
position: fixed;
margin: 0 auto;
background: rgba(0,0,0,0.4);
font-family: 'Lato', sans-serif;
color: #fff;
`;

const Navigation = styled.nav`
display: flex;
justify-content: space-between;
padding: 20px 80px;
width: 90%;
height: 100%;
align-items: center;
`;

const ButtonContainer = styled.div`
width: 30%;
padding: 0;
margin: 0;
display: flex;
justify-content: space-between;
`;

const UserMenuItem = styled(NavLink)`
text-decoration: none;
display: inline-block;
font-size: 26px;
color: #fff;
padding: 10px 20px;
cursor: pointer;
:hover{
box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
}
`

const UserMenu = styled.div`
display: flex;
width: 80%;
justify-content: space-between;
`

export { HeaderContainer, Navigation, ButtonContainer, UserMenuItem, UserMenu };