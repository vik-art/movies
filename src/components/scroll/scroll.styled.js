import styled from 'styled-components';

const ArrowContainer = styled.div`
display: inline-block;
width: 100px;
height: 100px;
position: fixed;
bottom: 40px;
right: 40px;
border-radius: 50%;
box-shadow: 0 14px 28px rgba(255,255,255,0.25), 0 10px 10px rgba(255,255,255,0.22);
cursor: pointer;
`;

export {ArrowContainer}