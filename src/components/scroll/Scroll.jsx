import React, { useEffect, useState } from "react";
import arrow from '../../assets/arrow.svg';
import {ArrowContainer} from './scroll.styled';

export default function Scroll() {
  const [isVisible, setIsVisible] = useState(false);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };

  useEffect(() => {
    const toggleVisibility = () => {
      if (window.pageYOffset > 500) {
        setIsVisible(true);
      } else {
        setIsVisible(false);
      }
    };

    window.addEventListener("scroll", toggleVisibility);

    return () => window.removeEventListener("scroll", toggleVisibility);
  }, []);

  return (
    <div className="scroll-to-top">
      {isVisible && (
        <ArrowContainer onClick={scrollToTop}>
          <img src={arrow} alt='Go up' width="100" height="100"/>
        </ArrowContainer>
      )}
    </div>
  );
}