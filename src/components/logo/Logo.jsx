import logoImage from '../../assets/movie-logo.png';
import { LogoContainer, StyledLink } from './logo.styled';

export const Logo = (props) => {
    return(
        <LogoContainer>
        <StyledLink to={props.link}>
            <img src= {logoImage} alt="logo" width="70" height="70"/>
            Your Shows
        </StyledLink>
        </LogoContainer>
    )
}