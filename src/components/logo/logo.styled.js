import styled from 'styled-components';
import { Link } from 'react-router-dom'

const LogoContainer = styled.div`
width: 120px;
text-align: center;
font-family: 'Dancing Script', cursive;
font-size: 25px;
color: #fff;
`;

const StyledLink = styled(Link)`
text-decoration: none;
display: flex;
align-items: center;
flex-direction: column;
color: #fff;
`;

export { LogoContainer, StyledLink}