import styled from 'styled-components';

const LoaderContainer = styled.div`
position: fixed;
top: 70%;
left: 50%;
transform: translateX(-50%);
`;

export {LoaderContainer}