import Loader from "react-loader-spinner";
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { Component } from 'react';
import {LoaderContainer} from './loader.styled'

class LoaderMark extends Component {
  render() {
    return (
      <LoaderContainer>
      <Loader
        type="Circles"
        color="rgb(255, 255, 255)"
        height={100}
        width={100}
        timeout={2000}
      />
      </LoaderContainer>
    );
  }
}
export default LoaderMark