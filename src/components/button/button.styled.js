import styled from 'styled-components';

const ButtonRef = styled.button`
display: inline-block;
background-color: #FFF44F;
box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
border: none;
border-radius: 3px;
color: #000;
padding: 10px 20px;
cursor: pointer;
font-size: 25px;
:hover {
    color: #fff;
    transition: color 1s .2s linear;
}
`;

export { ButtonRef };