import { ButtonRef } from './button.styled';

export const Button = (props) => {
    return(
        <ButtonRef onClick={props.func}>{props.text}</ButtonRef>
    )
}