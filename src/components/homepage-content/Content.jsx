import { NavLink } from '../../components';
import { Container, Title, Text } from './content.styled';

export const HomepageContent = () => {
    return(
        <Container>
        <Title>A TV-Show Lover’s Dream</Title>
        <Text>Classics and discoveries from around the world, thematically programmed with special features,<br/> on a streaming service brought to you by the Perfect Collection</Text>
        <NavLink to="/registration">Join now!</NavLink>
        </Container>
    )
}