import styled from 'styled-components';

const Container = styled.div`
width: 100%;
height: 100%;
display: flex;
align-items: center;
flex-direction: column;
justify-content: center;
`;

const Title = styled.h1`
margin: 0;
color: #FFF;
font-size: 72px;
`;

const Text = styled.p`
color: #FFF;
font-size: 25px;
text-align: center;
line-height: 1.5;
`

export { Container, Title, Text };