import selectedIcon from '../../assets/selected-icon.svg';
import likedIcon from '../../assets/liked-icon.svg';
import Scroll from '../scroll/Scroll';
import {Container, List, ListItem, ShowInfoContainer, ShowInfoItem, IconContainer, ShowImage, ShowInfoTitle, ButtonItem} from './results.styled';

const ShowsGallery = (props) => {
    return (
        <Container>
        <List>
            {props.arr.map(el => {
                const {id, image, name, rating, language, genres, status, type, summary} = el;
                if(image === null || name === null) {
                    return null;
                } else {
                return(
                    <ListItem key={id}>
                        <ShowInfoContainer>
                        <div>
                        <ShowImage 
                        src={image.original} 
                        alt={name} width="300"/>
                        </div>
                        <ShowInfoItem>
                        <ShowInfoTitle>{name}</ShowInfoTitle>
                            <p><strong>Rating:</strong> {rating.average}</p>
                            <p><strong>Language:</strong> {language}</p>
                            <p><strong>Genre:</strong> {genres[0]} </p>
                            <p><strong>Status:</strong> {status}</p>
                            <p><strong>Type:</strong> {type}</p>
                            <IconContainer>
                            <ButtonItem onClick={props.selectedFunc}>
                            <img src={selectedIcon} alt='select'/>
                            </ButtonItem>
                            <ButtonItem onClick={props.likedFunc}>
                            <img src={likedIcon} alt='like'/>
                            </ButtonItem>
                            </IconContainer>
                           </ShowInfoItem>
                        </ShowInfoContainer>
                        <p> {summary.replace(/(<([^>]+)>)/ig, '')}</p>
                    </ListItem>
                )
            }
        })
        
        }
        </List>
            <Scroll></Scroll>
    </Container> 
    )
}
export default ShowsGallery;