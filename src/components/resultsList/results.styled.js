import styled from 'styled-components';

const Container = styled.section`
background-color: #000;
padding: 30px 100px;
text-align: center;
`;

const List = styled.ul`
margin: 0;
padding: 0;
list-style-type: none;
display: flex;
justify-content: space-between;
flex-wrap: wrap;
color: #fff;
`;

const ListItem = styled.li`
width: 45%;
padding: 20px;
margin-bottom: 50px;
box-shadow: 0 14px 28px rgba(255,255,255,0.25), 0 10px 10px rgba(255,255,255,0.22);
`;

const ShowInfoContainer = styled.div`
display: flex;
justify-content: space-around;
`;

const ShowInfoItem = styled.div`
margin-left: 30px;
text-align: left;
flex-basis: 40%;
`;

const ShowInfoTitle = styled.h2`
margin-top: 0;
font-size: 2em;
`;

const IconContainer = styled.div`
width: 30%;
display: flex;
justify-content: space-between;
`;

const ShowImage = styled.img`
display: block;
`;

const ButtonItem = styled.button`
border: none;
background-color: transparent;
cursor: pointer;
`;



export {Container, List, ListItem, ShowInfoContainer, ShowInfoItem, IconContainer, ShowImage, ShowInfoTitle, ButtonItem}