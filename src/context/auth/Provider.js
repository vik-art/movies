import { useState } from 'react';
import authContext from './context';

export default function Provider({ children }) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const onLogIn = () => {
    setIsLoggedIn(true);
  };

  const onLogOut = () => {
    setIsLoggedIn(false);
  };

  const providerValue = { isLoggedIn, onLogIn, onLogOut };


  return (
    <authContext.Provider value={providerValue}>
      {children}
    </authContext.Provider>
  );
}