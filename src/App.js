import { Route, Switch, Redirect } from 'react-router-dom';
import { useContext } from 'react';

import authContext from './context/auth/context';
import { Homepage, Authorization, Registration, User, Favourite } from './pages'


export default function App() {

  const { isLoggedIn } = useContext(authContext);

  const PrivateRoute = ({component: Component, ...rest}) => {
    return (
        <Route {...rest} render={props => (
          isLoggedIn ?
                <Component {...props} />
            : <Redirect to="/" />
        )} />
    );
};

const PublicRoute = ({component: Component, restricted, ...rest}) => {
  return (
      <Route {...rest} render={props => (
        isLoggedIn ?
              <Redirect to="/user" />
          : <Component {...props} />
      )} />
  );
};

  return (
    <Switch>
          <PublicRoute component={Homepage} path="/" exact/>
          <PublicRoute component={Authorization} path="/login" exact/>
          <PublicRoute component={Registration} path="/registration"exact/>
          <PrivateRoute component={User} path="/user" exact/>
          <PrivateRoute component={Favourite} path="/favourite" exact/>
    </Switch>
  );
}