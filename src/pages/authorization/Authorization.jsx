import React, { useState, useContext } from 'react';

import {Logo, Form, Modal, Main, Container} from '../../components';

import homepageBg from '../../assets/homepage-background.jpg';

import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../libs/firebase";
import authContext from '../../context/auth/context';



export const Authorization = () => {
    const [logEmail, setLogEmail] = useState("");
    const [logPassword, setLogPassword] = useState("");
    const [showModal, setShowModal] = useState(false);

    const { onLogIn } = useContext(authContext);

    const toggleModal = () => {
        setShowModal(!setShowModal)
    }

    const loginSubmit = e => {
        e.preventDefault();
        console.log(logEmail, logPassword);
        signInWithEmailAndPassword( auth, logEmail, logPassword).then(cred => {
            onLogIn();
            console.log(cred)
        }).catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            setShowModal(true)
            console.log(errorCode, errorMessage)
          });
          setLogEmail("");
          setLogPassword("");
        }
        
    return(
        <Main homepage={homepageBg}>
            <Container>
            <Logo link="./"></Logo>
            <Form text="Log In!"
            func={loginSubmit}
            email={logEmail} 
            emailFunc={setLogEmail}
            password={logPassword}
            passwordFunc={setLogPassword} 
            >
            </Form>
            {showModal && <Modal 
            onClose={toggleModal}
            text="It appears that you didn't create a new account on Your Shows before. Please, come back to the registration. 
            If you did, make sure that your login and password are written correctly. Have a nice day!"
            modalText="Join now"
            link='./registration'
            >    
                </Modal>}
            </Container>          
        </Main>
    )
}