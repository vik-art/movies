import {Header, HomepageContent, Main} from '../../components';
import homepageBg from '../../assets/homepage-background.jpg';

export const Homepage = () => {
    return (
        <>
        <Header></Header>
        <Main homepage={homepageBg}>
            <HomepageContent></HomepageContent>
        </Main>
        </>
    )
}