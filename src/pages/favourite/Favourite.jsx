import {Header, Main} from '../../components';
import userBg from '../../assets/user-background.jpg';

export const Favourite = () => {
    return(
        <>
        <Header></Header>
        <Main user={userBg}></Main>
        </>
    )
}