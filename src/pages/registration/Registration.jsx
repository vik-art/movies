import { useState, useContext } from "react";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../libs/firebase";

import { Main, Container, Logo, Form, Modal } from '../../components';
import homepageBg from '../../assets/homepage-background.jpg'

import authContext from '../../context/auth/context';


export const Registration = () => {
    const [regEmail, setRegEmail] = useState("");
    const [regPassword, setRegPassword] = useState("");
    const [showWarning, setShowWarning] = useState(false);

    const { onLogIn } = useContext(authContext);

    const toggleModal = () => {
        setShowWarning(!setShowWarning)
    }

    const handleSubmit = e => {
        e.preventDefault();
        createUserWithEmailAndPassword( auth, regEmail, regPassword).then(cred => {
            onLogIn();
        }).catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            setShowWarning(true);
            console.log(errorCode, errorMessage)
          });
        setRegEmail("");
        setRegPassword("");
        }
        
    
    return(<Main homepage={homepageBg}>
        <Container>
        <Logo link="./"></Logo>
        <Form text="Sing Up!" 
        func={handleSubmit}
        email={regEmail} 
        emailFunc={setRegEmail}
        password={regPassword} 
        passwordFunc={setRegPassword}
        ></Form>
        {showWarning && <Modal onClose={toggleModal}
        text="It appears that you have already used this email on Your Shows before. Please, come back to the authorisation. 
        If you didn't, make sure that your login and password are written correctly. Have a nice day!"
        modalText="Log in!"
        link='./login'
        ></Modal>}
        </Container>
    </Main>
    )
}