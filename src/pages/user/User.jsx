import {Header, UserPageContent, Main} from '../../components';


export const User = () => {
    return(
        <>
            <Header></Header>
            <Main>
            <UserPageContent></UserPageContent>
        </Main>
        </>
    )
}