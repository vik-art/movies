const BASE_URL = 'https://api.tvmaze.com/';

function fetchShows (query) {
    return fetch(`${BASE_URL}search/shows?q=${query}`)
            .then(res => res.json())
}

export default fetchShows;

