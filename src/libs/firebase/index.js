import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from 'firebase/firestore/lite';

const firebaseConfig = {
  apiKey: "AIzaSyD95HyApe8CNYULrTIU_cmU3Otuuu1Hw3g",
  authDomain: "movie-ca6df.firebaseapp.com",
  projectId: "movie-ca6df",
  storageBucket: "movie-ca6df.appspot.com",
  messagingSenderId: "918419666218",
  appId: "1:918419666218:web:9b5c6571f1be1421c49a53"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth();
const db = getFirestore(app);

export { auth, db }